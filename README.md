# **Modelling a CardGame with Figaro**

## Overview
Suppose there are two decks of cards that contain the following cards:

|           |   Cards  |
|-----------|:--------:|
|  Deck_1   |   Spades(1,2,3,4,5,6) and Clubs(1,2,3,4,5,6)  |
|  Deck_2   |   Hearts(1,2,3,4,5,6)  |

Suppose that there are two players, PlayerA and PlayerB and the game play proceeds as follows:

1. From Deck_1:
  * Draw one card and place it face up.
  * Draw another card and deal it to PlayerA face down.
  * Draw another card and deal it to PlayerB face down.
2. From Deck_2:
  * Draw another card and deal it to PlayerA face down.
  * Draw another card and deal it to PlayerB face down.

Suppose you know PlayerA's 2nd card. At this point, you know the value of:
1. The face up card.
2. PlayerA's 2nd card from Deck_2.

PlayerA wins if `sum(playerA, 1st_card, 2nd_card)` >= `sum(playerB, 1st_card, 2nd_card)`. What is the probability of PlayerA winning?

## Set Up: Install the Simple Build Tool (SBT)
1. Download the version of SBT that you need from http://www.scala-sbt.org/download.html.
2. Open the installer and follow the instructions to install SBT.
3. Open up a command prompt.
4. Add the `sbt\bin` directory to your system `Path` variable using the appropriate method for your operating system.

## Running CardGame
1. Check out this repository
2. In the `./CardGame` directory, run the command
```bash
sbt
```
You should see something similar to the following:
```bash
[info] Loading project definition from /Users/xyz/Documents/CardGame/project
[info] Set current project to CardGame (in build file:/Users/xyz/Documents/CardGame/)
```
3. To run the CardGame project, enter:
```bash
run
```
4. You should see the following:
```bash
[info] Compiling 1 Scala source to /Users/annabeltay/Documents/CardGame/target/scala-2.12/classes...
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list
Multiple main classes detected, select one to run:

 [1] CardGame
 [2] CardGame2
```
Enter: `1`

5. You should then see some `[info]` statements followed by the following introduction:
```
Scenario: There are 2 decks of cards. 1st deck contains Spades 1-6 and Clubs 1-6, 2nd deck contains Hearts 1-6
From the 1st deck, draw 1 card and put it face up. What card is it?
Hint: Ace of Spades = S1, 2 of Clubs = C2
```
6. Follow the instructions and enjoy!
7. To exit, enter the command: `exit`

## Notes on CardGame2
CardGame2 is an experiment that shows the limitations of Figaro. Specifically, that Figaro cannot
handle a full deck of 52 cards. Running this probabilistic model will take around 400s and is expected
to result in a `java.lang.OutOfMemoryError`.
