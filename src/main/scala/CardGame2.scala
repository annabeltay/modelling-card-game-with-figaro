import com.cra.figaro.algorithm.factored._
import com.cra.figaro.language._
import com.cra.figaro.library.compound._
import com.cra.figaro.library.atomic._
import scala.io._

/**
 * Another Bayesian network example
 */
sealed trait Card2 extends Product with Serializable
final case object S1 extends Card2
final case object S2 extends Card2
final case object S3 extends Card2
final case object S4 extends Card2
final case object S5 extends Card2
final case object S6 extends Card2
final case object S7 extends Card2
final case object S8 extends Card2
final case object S9 extends Card2
final case object S10 extends Card2
final case object S11 extends Card2
final case object S12 extends Card2
final case object S13 extends Card2
final case object C1 extends Card2
final case object C2 extends Card2
final case object C3 extends Card2
final case object C4 extends Card2
final case object C5 extends Card2
final case object C6 extends Card2
final case object C7 extends Card2
final case object C8 extends Card2
final case object C9 extends Card2
final case object C10 extends Card2
final case object C11 extends Card2
final case object C12 extends Card2
final case object C13 extends Card2
final case object H1 extends Card2
final case object H2 extends Card2
final case object H3 extends Card2
final case object H4 extends Card2
final case object H5 extends Card2
final case object H6 extends Card2
final case object H7 extends Card2
final case object H8 extends Card2
final case object H9 extends Card2
final case object H10 extends Card2
final case object H11 extends Card2
final case object H12 extends Card2
final case object H13 extends Card2
final case object D1 extends Card2
final case object D2 extends Card2
final case object D3 extends Card2
final case object D4 extends Card2
final case object D5 extends Card2
final case object D6 extends Card2
final case object D7 extends Card2
final case object D8 extends Card2
final case object D9 extends Card2
final case object D10 extends Card2
final case object D11 extends Card2
final case object D12 extends Card2
final case object D13 extends Card2

object CardGame2 {
  Universe.createNew()
  private val deck1 = Array(
    S1, C1, H1, D1,
    S2, C2, H2, D2,
    S3, C3, H3, D3,
    S4, C4, H4, D4,
    S5, C5, H5, D5,
    S6, C6, H6, D6,
    S7, C7, H7, D7,
    S8, C8, H8, D8,
    S9, C9, H9, D9,
    S10, C10, H10, D10,
    S11, C11, H11, D11,
    S12, C12, H12, D12,
    S13, C13, H13, D13,
  )
  private def getIntValue(c: Card2) : Int = {
    c match {
      case S1 => 1
      case S2 => 2
      case S3 => 3
      case S4 => 4
      case S5 => 5
      case S6 => 6
      case S7 => 7
      case S8 => 8
      case S9 => 9
      case S10 => 10
      case S11 => 11
      case S12 => 12
      case S13 => 13
      case C1 => 1
      case C2 => 2
      case C3 => 3
      case C4 => 4
      case C5 => 5
      case C6 => 6
      case C7 => 7
      case C8 => 8
      case C9 => 9
      case C10 => 10
      case C11 => 11
      case C12 => 12
      case C13 => 13
      case H1 => 1
      case H2 => 2
      case H3 => 3
      case H4 => 4
      case H5 => 5
      case H6 => 6
      case H7 => 7
      case H8 => 8
      case H9 => 9
      case H10 => 10
      case H11 => 11
      case H12 => 12
      case H13 => 13
      case D1 => 1
      case D2 => 2
      case D3 => 3
      case D4 => 4
      case D5 => 5
      case D6 => 6
      case D7 => 7
      case D8 => 8
      case D9 => 9
      case D10 => 10
      case D11 => 11
      case D12 => 12
      case D13 => 13
    }
  }

  private val card1 = discrete.Uniform(deck1: _*)

  private val card2 = Chain(card1, (c1: Card2) => {
    val currentDeck = deck1.filterNot((c0: Card2) => c0 == c1)
    discrete.Uniform(currentDeck: _*)
  })
  private val card3 = Chain(card1, card2, (c1: Card2, c2: Card2) => {
    val currentDeck = deck1.filterNot((c0: Card2) => c0 == c1 || c0 == c2)
    discrete.Uniform(currentDeck: _*)
  })
  private val card1_greater_than_card3 = Chain(card1, card3,
    (c1: Card2, c3: Card2) => {
      if (getIntValue(c1) > getIntValue(c3)) {
        Constant(true)
      } else {
        Constant(false)
      }
  })
  def main(args: Array[String]) {
    card2.observe(H7)
    println("Calculating probability of card1 > card3.")
    val alg = VariableElimination(card1_greater_than_card3)
    alg.start()
    println("=> Probability of Player A winning is " + alg.probability(card1_greater_than_card3, true))
    alg.kill
  }
}
