import com.cra.figaro.algorithm.factored._
import com.cra.figaro.language._
import com.cra.figaro.library.compound._
import com.cra.figaro.library.atomic._
import scala.io._

/**
 * Another Bayesian network example
 */
sealed trait Card extends Product with Serializable
final case object Spades1 extends Card
final case object Spades2 extends Card
final case object Spades3 extends Card
final case object Spades4 extends Card
final case object Spades5 extends Card
final case object Spades6 extends Card
final case object Clubs1 extends Card
final case object Clubs2 extends Card
final case object Clubs3 extends Card
final case object Clubs4 extends Card
final case object Clubs5 extends Card
final case object Clubs6 extends Card
final case object Hearts1 extends Card
final case object Hearts2 extends Card
final case object Hearts3 extends Card
final case object Hearts4 extends Card
final case object Hearts5 extends Card
final case object Hearts6 extends Card

 object CardGame {
   Universe.createNew()
   private val deck1 = Array(
     Spades1, Clubs1,
     Spades2, Clubs2,
     Spades3, Clubs3,
     Spades4, Clubs4,
     Spades5, Clubs5,
     Spades6, Clubs6,
   )
   // The element discrete.Uniform takes as arguments,
   // an explicit sequence of values of variable length.
   // Since deck1 is a single field that represents an array, it must be
   // converted it into a sequence of arguments with the `deck1: _*` notation.
   // card1_faceup is therefore one of the deck of cards chosen randomly.
   private val card1_faceup = discrete.Uniform(deck1: _*)

   private val card2_playerA_1 = Chain(card1_faceup, (c1: Card) => {
     val currentDeck = deck1.filterNot((c0: Card) => c0 == c1)
     discrete.Uniform(currentDeck: _*)
   })
   private val card3_playerB_1 = Chain(card1_faceup, card2_playerA_1, (c1: Card, c2: Card) => {
     val currentDeck = deck1.filterNot((c0: Card) => c0 == c1 || c0 == c2)
     discrete.Uniform(currentDeck: _*)
   })

   private val deck2 = Array(
       Hearts1, Hearts2, Hearts3, Hearts4, Hearts5, Hearts6
   )
   private val card4_playerA_2 = discrete.Uniform(deck2: _*)
   private val card5_playerB_2 = Chain(card4_playerA_2, (c1: Card) => {
     val currentDeck = deck2.filterNot((c0: Card) => c0 == c1)
     discrete.Uniform(currentDeck: _*)
   })

   private val playerA_cardValue = Chain(card2_playerA_1, card4_playerA_2, (c1: Card, c2: Card) => {
     Constant(sumUpValues(c1, c2))
   })
   private val playerB_cardValue = Chain(card3_playerB_1, card5_playerB_2, (c1: Card, c2: Card) => {
     Constant(sumUpValues(c1,c2))
   })

   private val playerA_wins = Chain(playerA_cardValue, playerB_cardValue,
     (playerA: Int, playerB: Int) => {
       if (playerB > playerA) {
         Constant(false)
       } else {
         Constant(true)
       }
     }
   )

   private def getIntValue(c: Card) : Int = {
     c match {
       case Spades1 => 1
       case Spades2 => 2
       case Spades3 => 3
   		 case Spades4 => 4
   	   case Spades5 => 5
   		 case Spades6 => 6
   		 case Clubs1 => 1
   		 case Clubs2 => 2
   		 case Clubs3 => 3
   		 case Clubs4 => 4
   		 case Clubs5 => 5
   		 case Clubs6 => 6
   		 case Hearts1 => 1
   		 case Hearts2 => 2
   		 case Hearts3 => 3
   		 case Hearts4 => 4
   		 case Hearts5 => 5
   		 case Hearts6 => 6
     }
   }

   private def sumUpValues(c1: Card, c2: Card) : Int = {
     getIntValue(c1) + getIntValue(c2)
   }

   private def printIntroduction {
     println("Scenario: There are 2 decks of cards. 1st deck contains Spades 1-6 and Clubs 1-6, 2nd deck contains Hearts 1-6")
     println("From the 1st deck, draw 1 card and put it face up. What card is it?")
     println("Hint: Ace of Spades = S1, 2 of Clubs = C2")
   }

   private def getCard(suit: Char, faceValue: Int, cardDeck: Int): Option[Card] = {
     if (cardDeck == 1 && !(suit == 's' || suit == 'c')) {
       println("Deck 1 only contains Spades and Clubs")
       // return must be used here to prevent the execution of the pattern matching code segment
       return None
     }
     if (cardDeck == 2 && suit != 'h') {
       // return must be used here to prevent the execution of the pattern matching code segment
       println("Deck 2 only contains Hearts")
       return None
     }
     faceValue match {
       case 1 => suit match {
         case 's' => Some(Spades1)
         case 'c' => Some(Clubs1)
         case 'h' => Some(Hearts1)
         case _ => None
       }
       case 2 => suit match {
         case 's' => Some(Spades2)
         case 'c' => Some(Clubs2)
         case 'h' => Some(Hearts2)
         case _ => None
       }
       case 3 => suit match {
         case 's' => Some(Spades3)
         case 'c' => Some(Clubs3)
         case 'h' => Some(Hearts3)
         case _ => None
       }
       case 4 => suit match {
         case 's' => Some(Spades4)
         case 'c' => Some(Clubs4)
         case 'h' => Some(Hearts4)
         case _ => None
       }
       case 5 => suit match {
         case 's' => Some(Spades5)
         case 'c' => Some(Clubs5)
         case 'h' => Some(Hearts5)
         case _ => None
       }
       case 6 => suit match {
         case 's' => Some(Spades6)
         case 'c' => Some(Clubs6)
         case 'h' => Some(Hearts6)
         case _ => None
       }
       case _ => {
         println("Valid face value of cards: 1,2,3,4,5,6")
         None
       }
     }
   }

   private def stringToCard(s: String, cardDeck: Int): Option[Card] = {
     val s1 = s.toLowerCase().trim()
     if (s == "" || s.length() != 2) None
     else {
       val suit = s1(0)
       val faceValue = s1(1).asDigit
       getCard(suit, faceValue, cardDeck)
     }
   }

   private def inputToCard(cardDeck: Int, selectedCards: Option[Array[Card]]): Card = {
     val line = StdIn.readLine()
     stringToCard(line, cardDeck) match {
       case Some(c) => selectedCards match {
         case None => c
         case Some(arr) => if (arr.contains(c)) {
           println("The decks don't contain repeated cards. What card did player B get?")
           return inputToCard(cardDeck, selectedCards)
         } else {
           return c
         }
       }
       case None => {
         println("Invalid input! Hint: Ace of Spades = S1, 2 of Clubs = C2")
         inputToCard(cardDeck, selectedCards)
       }
     }
   }

   def main(args: Array[String]) {
     printIntroduction
     val card_1 = inputToCard(1, None)
     println("=> Face up card is " + card_1 + "\n")
     println("From the 1st deck, deal a card to player A face down. Deal another card to player B face down.\n")
     println("From the 2nd deck, deal a card to player A and player B each. What card did player A get?")
     val card_4 = inputToCard(2, None)
     println("=> Player A's 2nd card is " + card_4 + "\n")

     card1_faceup.observe(card_1)
     card4_playerA_2.observe(card_4)
     println("Calculating probability of PlayerA winning... This usually takes a while...")
     val alg = VariableElimination(playerA_wins)
     alg.start()
     println("=> Probability of Player A winning is " + alg.probability(playerA_wins, true))
     alg.kill
   }


 }
