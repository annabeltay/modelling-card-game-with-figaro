import sbt._
import Keys._

  lazy val cardGameSettings =  Seq(
    scalaVersion := "2.12.2",
    retrieveManaged := true
  )

  lazy val cardGame = Project("CardGame", file("."))
    .settings(cardGameSettings)
    .settings (scalacOptions ++= Seq(
	"-feature",
	"-language:existentials",
	"-deprecation",
	"-language:postfixOps"
    ))
    .settings(libraryDependencies ++= Seq(
      "com.cra.figaro" %% "figaro" % "latest.release"
    ))
